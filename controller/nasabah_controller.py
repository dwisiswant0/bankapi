from account.nasabah import Nasabah
from module.keys import Keys

class Nasabah_Controller:

    def __init__(self, apikey):
        self.apikey = apikey

    def Create(self, username, password, email, phone, address, firstname, lastname):
        nasabah = Nasabah()

        keys = Keys()
        norek = ''
        if keys.isExists(self.apikey):
            status, messages, norek = nasabah.Create(username, password, email, phone, address, firstname, lastname)
        else:
            status = 'Failed'
            messages = 'Apikey is invalid'
        
        profile = {
            'username': username,
            'email': email,
            'phone': phone,
            'name': firstname + " " + lastname,
            'no_rekening': norek
        }
        data = {
            'status_info': status,
            'status_messages': messages,
            'profile': profile
        }

        return data

    def Login(self, username, password):
        nasabah = Nasabah()

        keys = Keys()
        if keys.isExists(self.apikey):
            status, messages = nasabah.Login(username, password)
        else:
            status = 'Failed'
            messages = 'Apikey is invalid'
        
        data = {
            'status_info': status,
            'status_messages': messages
        }

        return data