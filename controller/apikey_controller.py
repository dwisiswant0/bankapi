from module.keys import Keys

class Apikey_Controller:

    def __init__(self, secret_key):
        self.secret_key = secret_key

    def Generate(self):
        keys = Keys()
        status, messages, apikey = keys.generateApiKeys(self.secret_key)
        data = {
            'status_info': status,
            'status_messages': messages,
            'apikey': apikey
        }
        return data