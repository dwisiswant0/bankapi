import pymysql.cursors
from module.conn import Connection
from module.crypter import Crypter
from random import randint

class Keys:

    def __init__(self):
        con = Connection()
        self.connection = con.get()

    def isExists(self, keys):
        sql = 'SELECT * FROM credentials WHERE apikey = %s'
        cursor = self.connection.cursor()
        #Execute
        cursor.execute(sql, keys)

        data = cursor.fetchone()
        if data == None:
            return False
        else:
            return True

    def generateApiKeys(self, secretkey):
        num = randint(1000000000000000, 9999999999999999)
        payload = str(num)
        crypt = Crypter(payload, secretkey)
        keys = crypt.Encrypt()

        keys = keys.replace(b'++', b'Y0')

        #Jika apikey belum ada
        if not self.isExists(keys):
            if secretkey == '1129874678923456':
                sql = 'INSERT INTO credentials (apikey) VALUES(%s)'
                cursor = self.connection.cursor()
                #Execute
                cursor.execute(sql, keys)
                self.connection.commit()

                return 'Success', 'Successfully generate apikey', keys
            else:
                return 'Failed', 'Secret key is not correct', ''
            
