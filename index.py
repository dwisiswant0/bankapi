import flask
from flask import request, jsonify
from controller.nasabah_controller import Nasabah_Controller
from controller.apikey_controller import Apikey_Controller
from controller.saldo_controller import Saldo_Controller
from controller.transaction_controller import Transaction_Controller

app = flask.Flask(__name__)
app.config['DEBUG'] = True

'''

API Name: Bank API
Description: API ini berfungsi sebagai api banking system yang mempunyai fitur
        - Create Nasabah Account
        - Setor Saldo
        - Penarikan Saldo
        - Transfer Saldo
        - Nomor Rekening (15 Digit Angka)
        - Check Rekening (Personal or All)
        - Login & Register System
        - Apikey System
Untuk sisi keamanan Password & Generate Apikey menggunakan Block chiper AES 16 byte.

Rute API:
Main Route -> Controller -> Core System
After Success Get Data:
Core System -> Controller -> Main Route -> Display

Fungsi Controller:
Controller -> Menentukan Rute, Validasi Apikey, mengambil data, mengirim ke Main Route

Author: Yusril Rapsanjani
Version: v1.0
Website: www.yurani.me
-----------------------------------------------------------------------------

'''



'''

Rute ini berfungsi untuk melakukan generate APIKEY.
Untuk melakukannya, kita perlu memasukkan parameter secret key.
Hal ini mencegah dari orang yang membypass generate apikey, jadi
hanya orang tertentu yang tau secret key nya saja yang bisa generate apikey

Secret key => 1129874678923456
Parameter required:
        - secret_key

'''
@app.route('/api/v1/keys/generate', methods=['POST'])
def generateToken():

    if 'secret_key' in request.args:
        secret = request.args['secret_key']
        controller = Apikey_Controller(secret)
        data = controller.Generate()

        return jsonify(data)

'''

Rute ini berfungsi untuk membuat akun nasabah.
Perlu diingatkan bahwa kalian memerlukan APIKEY.
Parameter required:
        - username
        - password
        - email
        - phone
        - address
        - firstname
        - lastname
        - apikey

'''
@app.route('/api/v1/nasabah/create', methods=['POST'])
def createNasabah():

    #Validasi parameter
    if 'username' in request.args and 'password' in request.args and 'email' in request.args and 'phone' in request.args and 'address' in request.args and 'firstname' in request.args and 'lastname' in request.args and 'keys' in request.args:
        username = request.args['username']
        password = request.args['password']
        email = request.args['email']
        phone = request.args['phone']
        address = request.args['address']
        firstname = request.args['firstname']
        lastname = request.args['lastname']
        apikey = request.args['keys']

        controller = Nasabah_Controller(apikey)
        data = controller.Create(username, password, email, phone, address, firstname, lastname)

        return jsonify(data)

'''

Rute ini untuk melakukan validasi login.
Pastikan kalian memasukan parameter keys, karena
kita tidak ingin validasi ini digunakan sembarangan orang.
Required parameter:
    - username
    - password
    - keys

'''
@app.route('/api/v1/nasabah/login', methods=['GET'])
def loginNasabah():
    if 'keys' in request.args and 'username' in request.args and 'password' in request.args:
        username = request.args['username']
        password = request.args['password']
        apikey = request.args['keys']

        controller = Nasabah_Controller(apikey)
        data = controller.Login(username, password)

        return jsonify(data)

'''

Rute ini untuk melakukan deposit uang ke dalam akun nasabah.
Pastikan bahwa nomor rekening benar.
Nomor Rekening terdiri dari 15 Digit
Parameter required:
        - norek
        - keys
        - amount

'''
@app.route('/api/v1/nasabah/deposit', methods=['POST'])
def deposit():
        if 'norek' in request.args and 'keys' in request.args and 'amount' in request.args:
                norek = request.args['norek']
                apikey = request.args['keys']
                amount = int(request.args['amount'])

                controller = Transaction_Controller(apikey)
                data = controller.Insert(norek, amount)

                return jsonify(data)


'''

Rute ini untuk melakukan withdraw uang atau pengambilan uang
dari akun nasabah. Pastikan bahwa nomor rekening benar.
Parameter required:
        - norek
        - keys
        - amount

'''
@app.route('/api/v1/nasabah/withdraw', methods=['POST'])
def withdraw():
        if 'norek' in request.args and 'keys' in request.args and 'amount' in request.args:
                norek = request.args['norek']
                apikey = request.args['keys']
                amount = int(request.args['amount'])

                controller = Transaction_Controller(apikey)
                data = controller.Take(norek, amount)

                return jsonify(data)

'''

Rute ini berfungsi untuk melakukan transfer saldo antara nasabah.
Pastikan bahwa nomor rekening pengirim dan nomor rekening tujuan benar.
Parameter required:
        - from_norek
        - to_norek
        - amount
        - notes
        - keys

'''
@app.route('/api/v1/nasabah/transfer', methods=['POST'])
def transfer():
        if 'from_norek' in request.args and 'to_norek' in request.args and 'amount' in request.args and 'notes' in request.args and 'keys' in request.args:
                from_norek = request.args['from_norek']
                to_norek = request.args['to_norek']
                amount = int(request.args['amount'])
                notes = request.args['notes']
                apikey = request.args['keys']
                
                controller = Transaction_Controller(apikey)
                data = controller.Send(from_norek, to_norek, amount, notes)
                
                return jsonify(data)

'''

Rute ini berfungsi untuk melakukan pengecekan rekening. 
Dengan memasukkan nomor rekening dan apikey, kita bisa melihat
detail informasi nasabah seperti profile dan saldonya.
Parameter required:
        - norek
        - keys

'''
@app.route('/api/v1/nasabah/check', methods=['GET'])
def checkRekening():
        if 'norek' in request.args and 'keys' in request.args:
                norek = request.args['norek']
                apikey = request.args['keys']

                controller = Saldo_Controller(norek, apikey)
                data = controller.Check()

                return jsonify(data)

@app.route('/api/v1/nasabah/checkall', methods=['GET'])
def checkRekeningAll():
        if 'keys' in request.args:
                apikey = request.args['keys']

                controller = Saldo_Controller('', apikey)
                data = controller.CheckAll()

                return jsonify(data)

#Run aplikasi
app.run()