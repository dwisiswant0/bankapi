import pymysql.cursors
from module.conn import Connection
from module.keys import Keys

class Saldo:

    def __init__(self):
        con = Connection()
        self.connection = con.get()

    def Check(self, norek):
        cursor = self.connection.cursor()
        cursor.execute("SELECT concat(na.firstname , ' ' , na.lastname) as name, na.phone, na.address, na.email, na.norek, sd.saldo FROM nasabah na INNER JOIN saldo sd ON na.norek = sd.norek WHERE na.norek = %s", norek)
        data = cursor.fetchone()

        if data != None:
            return 'Success', 'Successfully view nasabah profile', data
        else:
            return 'Failed', 'Nomor Rekening is invalid', ''

    def CheckAll(self):
        cursor = self.connection.cursor()
        cursor.execute("SELECT concat(na.firstname , ' ' , na.lastname) as name, na.phone, na.address, na.email, na.norek, sd.saldo FROM nasabah na INNER JOIN saldo sd ON na.norek = sd.norek")
        data = cursor.fetchall()

        if data != None:
            return 'Success', 'Successfully view nasabah profile', data
        else:
            return 'Failed', 'Nomor Rekening is invalid', ''

    def Update(self, norek, saldo):
        cursor = self.connection.cursor()
        cursor.execute("UPDATE saldo SET saldo = %s WHERE norek = %s", (saldo, norek))
        self.connection.commit()

    def CreateInitialize(self, norek):
        cursor = self.connection.cursor()

        value = 0
        cursor.execute('INSERT INTO saldo (norek, saldo) values(%s, %s)', (norek, value))
        self.connection.commit()